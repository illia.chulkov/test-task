#Info

To deploy the project, you need (for Linux)
* In file /etc/host add line

```
127.0.0.1 test.site.dev
```

* After in terminal, execute two commands in folder webserver

```bash
docker-compose build
docker-compose up -d
```

* Link https://test.site.dev
---
* To build project, run command

```bash
bash command/install.sh
```
---
#API
* Add task https://test.site.dev/api/add
* Type POST
```json
{
  "title": "Task 1",
  "description": "Description",
  "status": "todo",
  "priority": "1"
}
```
---
* Remove task https://test.site.dev/api/remove/{id}
* {id} - where id task
* Type GET
---
* Set status done https://test.site.dev/api/done/{id}
* {id} - where id task
* Type GET
---
* Update task https://test.site.dev/api/update
* Type POST
```json
{
    "id": "6",
    "title": "Task 6",
    "description": "Description",
    "status": "todo",
    "priority": "5"
}
```
---
* List task https://test.site.dev/api/list
* Type GET
* Filter parameters
* status=done
* priority[from]=1&priority[to]=5
* title=word
* sort=priority
* Example https://test.site.dev/api/list?status=done&priority[from]=1&priority[to]=5
---
* Add parent task https://test.site.dev/api/parent/{taskId}/{parentId}
* {taskId} - child task
* {parentId} - parent task

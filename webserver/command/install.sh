#!/bin/bash

docker-compose exec php php -d memory_limit=-1 /usr/bin/composer i
docker-compose exec php bin/console doctrine:schema:update --force

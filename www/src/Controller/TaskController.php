<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\TaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 * @package App\Controller
 * @Route("/api", name="post_api")
 */
class TaskController extends AbstractController
{
    /**
     * @var TaskService
     */
    private TaskService $taskService;

    /**
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/list", name="list", methods={"GET"})
     */
    public function listAction(Request $request): JsonResponse
    {
        $result = $this->taskService->getListTask($request->query->all());
        return new JsonResponse($result['result'], $result['status']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/add", name="add", methods={"POST"})
     */
    public function addAction(Request $request): JsonResponse
    {
        $result = $this->taskService->addTask($request);
        return new JsonResponse($result, $result['status']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @Route("/remove/{id}", name="remove", methods={"GET"})
     */
    public function removeAction(int $id): JsonResponse
    {
        $result = $this->taskService->removeTask($id);
        return new JsonResponse($result, $result['status']);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @Route("/done/{id}", name="done", methods={"GET"})
     */
    public function doneAction(int $id): JsonResponse
    {
        $result = $this->taskService->setStatusTask($id, 'done');
        return new JsonResponse($result, $result['status']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/update", name="update", methods={"POST"})
     */
    public function updateAction(Request $request): JsonResponse
    {
        $result = $this->taskService->updateTask($request);
        return new JsonResponse($result, $result['status']);
    }

    /**
     * @param int $taskId
     * @param int $parentId
     * @return JsonResponse
     * @Route("/parent/{taskId}/{parentId}", name="parent", methods={"GET"})
     */
    public function parentAction(int $taskId, int $parentId): JsonResponse
    {
        $result = $this->taskService->setParentTask($taskId, $parentId);
        return new JsonResponse($result, $result['status']);
    }
}

<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\TaskRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTime $createDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTime $updateDate;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $status;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $priority;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $title;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $description;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="parent", cascade={"remove"})
     */
    private ?PersistentCollection $children;

    /**
     * @ORM\ManyToOne(targetEntity="Task", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     */
    protected ?Task $parent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreateDate(new \DateTime());
        $this->setUpdateDate(new \DateTime());
        $this->setStatus('');
        $this->setPriority(0);
        $this->setTitle('');
        $this->setDescription('');
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return DateTime|null
     */
    public function getCreateDate(): ?DateTime
    {
        return $this->createDate;
    }

    /**
     * @param DateTime $createDate
     */
    public function setCreateDate(DateTime $createDate): void
    {
        $this->createDate = $createDate;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdateDate(): ?DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param DateTime $updateDate
     */
    public function setUpdateDate(DateTime $updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return PersistentCollection|null
     */
    public function getChildren(): ?PersistentCollection
    {
        return $this->children;
    }

    /**
     * @param PersistentCollection $children
     * @return void
     */
    public function setChildren(PersistentCollection $children): void
    {
        $this->children = $children;
    }

    /**
     * @param Task $child
     * @return void
     */
    public function addChild(Task $child): void
    {
        $this->children[] = $child;
        $child->setParent($this);
    }

    /**
     * @return Task|null
     */
    public function getParent(): ?Task
    {
        return $this->parent;
    }

    /**
     * @param Task $parent
     * @return void
     */
    public function setParent(Task $parent): void
    {
        $this->parent = $parent;
    }
}

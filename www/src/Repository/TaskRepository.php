<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TaskRepository
 */
class TaskRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @param array $condition
     * @return array
     */
    public function findByParam(array $condition): array
    {
        $queryBuilder = $this->createQueryBuilder('t');

        if (isset($condition['status'])) {
            $queryBuilder->andWhere('t.status = :status')->setParameter('status', $condition['status']);
        }

        if (isset($condition['priority']['from']) && isset($condition['priority']['to'])) {
            $queryBuilder->andWhere('t.priority >= :from')
                ->andWhere('t.priority <= :to')
                ->setParameters(['from' => $condition['priority']['from'], 'to' => $condition['priority']['to']]);
        }

        if (isset($condition['title'])) {
            $queryBuilder->andWhere('t.title LIKE :title')->setParameter('title', '%' . $condition['title'] . '%');
        }

        if (isset($condition['sort'])) {
            $queryBuilder->addOrderBy('t.' . $condition['sort']);
        }

        return $queryBuilder->getQuery()->getArrayResult();
    }
}

<?php
declare(strict_types=1);

namespace App\Service;

use App\Repository\TaskRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Abstract class AbstractService
 */
abstract class AbstractService
{
    const STATUS_TODO = 'todo';

    const STATUS_DONE = 'done';

    const CODE_200 = 200;

    const CODE_422 = 422;

    const CODE_404 = 404;

    /**
     * @var Serializer
     */
    protected Serializer $serializer;

    /**
     * @var ValidatorService
     */
    protected ValidatorService $validatorService;

    /**
     * @var ManagerRegistry
     */
    protected ManagerRegistry $doctrine;

    /**
     * @var TaskRepository
     */
    protected TaskRepository $taskRepository;

    /**
     * @var ObjectManager
     */
    protected ObjectManager $entityManager;

    /**
     * @param ManagerRegistry $doctrine
     * @param ValidatorService $validatorService
     * @param TaskRepository $taskRepository
     */
    public function __construct(
        ManagerRegistry $doctrine,
        ValidatorService $validatorService,
        TaskRepository $taskRepository
    ) {
        $this->serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $this->doctrine = $doctrine;
        $this->validatorService = $validatorService;
        $this->taskRepository = $taskRepository;
        $this->entityManager = $this->doctrine->getManager();
    }
}

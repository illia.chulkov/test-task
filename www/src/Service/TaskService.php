<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Task;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TaskService
 */
class TaskService extends AbstractService
{
    /**
     * @param Request $request
     * @return array
     */
    public function addTask(Request $request): array
    {
        if ($request->headers->get('Content-Type') !== 'application/json') {
            return [
                'status' => self::CODE_422,
                'errors' => 'Content-Type must be json'
            ];
        }

        /** @var Task $task */
        $task = $this->serializer->deserialize($request->getContent(), Task::class, 'json');
        $validate = $this->validatorService->validate($task);

        if ($validate['status'] === false) {
            return [
                'status' => self::CODE_422,
                'errors' => $validate['message']
            ];
        }

        $this->entityManager->persist($task);
        $this->entityManager->flush();

        if ($task->getId()) {
            $result = [
                'status' => self::CODE_200,
                'success' => 'Added new task',
                'id' => $task->getId()
            ];
        } else {
            $result = [
                'status' => self::CODE_422,
                'errors' => 'Task not added'
            ];
        }

        return $result;
    }

    /**
     * @param int $id
     * @return array
     */
    public function removeTask(int $id): array
    {
        /** @var Task $task */
        $task = $this->doctrine->getRepository(Task::class)->find($id);

        if (!$task) {
            return [
                'status' => self::CODE_404,
                'errors' => 'Task not found'
            ];
        }

        if ($this->hasOpenSubTask($task)) {
            return [
                'status' => self::CODE_404,
                'errors' => 'Sub task not close'
            ];
        }

        if ($task->getStatus() === self::STATUS_DONE) {
            return [
                'status' => self::CODE_422,
                'errors' => 'The task has already been completed'
            ];
        }

        $this->entityManager->remove($task);
        $this->entityManager->flush();

        return [
            'status' => self::CODE_200,
            'success' => 'Task removed'
        ];
    }

    /**
     * @param int $id
     * @param string $status
     * @return array
     */
    public function setStatusTask(int $id, string $status): array
    {
        /** @var Task $task */
        $task = $this->taskRepository->find($id);

        if (!$task) {
            return [
                'status' => self::CODE_404,
                'errors' => 'Task not found'
            ];
        }

        $task->setStatus($status);
        $validate = $this->validatorService->validate($task);

        if ($validate['status'] === false) {
            return [
                'status' => self::CODE_422,
                'errors' => $validate['message']
            ];
        }

        $this->entityManager->flush();

        return [
            'status' => self::CODE_200,
            'errors' => 'Task update'
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function updateTask(Request $request): array
    {
        if ($request->headers->get('Content-Type') !== 'application/json') {
            return [
                'status' => self::CODE_422,
                'errors' => 'Content-Type must be json'
            ];
        }

        /** @var Task $task */
        $task = $this->serializer->deserialize($request->getContent(), Task::class, 'json');

        if (!$task->getId()) {
            return [
                'status' => self::CODE_422,
                'errors' => 'Task not have id'
            ];
        }

        $validate = $this->validatorService->validate($task);

        if ($validate['status'] === false) {
            return [
                'status' => self::CODE_422,
                'errors' => $validate['message']
            ];
        }

        $this->entityManager->merge($task);
        $this->entityManager->flush();

        return [
            'status' => self::CODE_200,
            'success' => 'Task update',
            'id' => $task->getId()
        ];
    }

    /**
     * @param array $condition
     * @return array
     */
    public function getListTask(array $condition): array
    {
        return [
            'status' => self::CODE_200,
            'result' => $this->taskRepository->findByParam($condition)
        ];
    }

    /**
     * @param int $taskId
     * @param int $parentId
     * @return array
     */
    public function setParentTask(int $taskId, int $parentId): array
    {
        $parentTask = $this->taskRepository->find($parentId);

        if (!$parentTask) {
            return [
                'status' => self::CODE_422,
                'errors' => 'Parent task not found'
            ];
        }

        $task = $this->taskRepository->find($taskId);

        if (!$task) {
            return [
                'status' => self::CODE_422,
                'errors' => 'Task not found'
            ];
        }

        $task->setParent($parentTask);
        $this->entityManager->flush();

        return [
            'status' => self::CODE_200,
            'success' => 'Task update'
        ];
    }

    /**
     * @param Task $task
     * @return bool
     */
    protected function hasOpenSubTask(Task $task): bool
    {
        /** @var Task $subTask */
        foreach ($task->getChildren() as $subTask) {
            if ($subTask->getStatus() === self::STATUS_TODO) {
                return true;
            }

            if ($subTask->getChildren()->count() === 0) {
                continue;
            }

            if ($this->hasOpenSubTask($subTask)) {
                return true;
            }
        }

        return false;
    }
}

<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Task;
use App\Validator\PriorityValidator;
use App\Validator\StatusValidator;
use App\Validator\ValidatorInterface;

class ValidatorService
{
    /**
     * @var array|string[]
     */
    protected array $listValidator = [
        PriorityValidator::class,
        StatusValidator::class
    ];

    /**
     * @param Task $task
     * @return array
     */
    public function validate(Task $task): array
    {
        foreach ($this->listValidator as $validator) {
            /** @var ValidatorInterface $validatorObject */
            $validatorObject = new $validator();
            if ($validatorObject->validate($task) === false) {

                return [
                    'status' => false,
                    'message' => $validatorObject->getMessage()
                ];
            }
        }

        return ['status' => true];
    }
}

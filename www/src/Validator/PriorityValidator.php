<?php
declare(strict_types=1);

namespace App\Validator;

use App\Entity\Task;

class PriorityValidator implements ValidatorInterface
{
    /**
     * @param Task $task
     * @return bool
     */
    public function validate(Task $task): bool
    {
        if ($task->getPriority() >=0 && $task->getPriority() <=5) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'Invalid priority';
    }
}

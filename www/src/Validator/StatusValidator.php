<?php
declare(strict_types=1);

namespace App\Validator;

use App\Entity\Task;

class StatusValidator implements ValidatorInterface
{
    /**
     * @param Task $task
     * @return bool
     */
    public function validate(Task $task): bool
    {
        if ($task->getStatus() === 'todo' || $task->getStatus() === 'done') {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'Invalid status';
    }
}

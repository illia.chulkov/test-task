<?php
declare(strict_types=1);

namespace App\Validator;

use App\Entity\Task;

interface ValidatorInterface
{
    public function validate(Task $task): bool;

    public function getMessage(): string;
}
